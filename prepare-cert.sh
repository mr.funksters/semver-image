#!/bin/bash -e

CERT="$1"
TMP_CERT="tmp/tmp-cert.pem"

if [ ! -d tmp ]
then
  mkdir tmp
fi

if [ -f $CERT ] 
then
  sed -i 's/-----BEGIN RSA PRIVATE KEY----- //' $CERT
  sed -i 's/ -----END RSA PRIVATE KEY-----//' $CERT
  tr " " "\n" < $CERT >> $TMP_CERT
  rm -f $CERT
  sed -i '1s/^/-----BEGIN RSA PRIVATE KEY-----\n/' $TMP_CERT
  echo "-----END RSA PRIVATE KEY-----" >> $TMP_CERT
  cat $TMP_CERT >> $CERT
  echo "SSH Cert is builded!"
fi

rm -r tmp